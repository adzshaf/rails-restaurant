Rails.application.routes.draw do
  resources :categories
  get 'categories/new', as: 'categories_new'
  get 'categories/:id', to: 'categories#show'
  get 'categories/:id/edit', to: 'categories#edit'
  post 'categories', to: 'categories#create'
  delete 'categories/:id', to: 'categories#destroy'
  patch 'categories/:id', to: 'categories#update', as: 'update_category'
  get 'categories', to: 'categories#index'

  resources :menus
  get '', to: 'menus#index'
  get 'menus', to: 'menus#index'
  get 'menus/new', to: 'menus#new'
  post 'menus', to: 'menus#create'
  delete 'menus/:id', to: 'menus#destroy'
  get 'menus/:id', to: 'menus#show'
  get 'menus/:id/edit', to: 'menus#edit'
  patch 'menus/:id', to: 'menus#update', as: 'update_menu'

  resources :customers
  get 'customers', to: 'customers#index'
  get 'customers/new', to: 'customers#new'
  post 'customers', to: 'customers#create'
  delete 'customers/:id', to: 'customers#destroy'
  get 'customers/:id', to: 'customers#show'
  get 'customers/:id/edit', to: 'customers#edit'
  patch 'customers/:id', to: 'customers#update', as: 'update_customers'

  resources :orders
  get 'orders', to: 'orders#index'
  get 'orders/new', to: 'orders#new'
  get 'orders/new/:id', to: 'orders#new_by_customer', as: 'get_order_customer'
  post 'orders/:id', to: 'orders#create'
  delete 'orders/:id', to: 'orders#destroy'
  get 'orders/:id', to: 'orders#show'
  get 'orders/:id/edit', to: 'orders#edit'
  patch 'orders/:id', to: 'orders#update', as: 'update_orders'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
