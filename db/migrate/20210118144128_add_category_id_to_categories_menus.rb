class AddCategoryIdToCategoriesMenus < ActiveRecord::Migration[6.1]
  def change
    add_column :categories_menus, :category_id, :integer
  end
end
