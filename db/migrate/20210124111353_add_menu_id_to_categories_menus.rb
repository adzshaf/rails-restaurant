class AddMenuIdToCategoriesMenus < ActiveRecord::Migration[6.1]
  def change
    add_column :categories_menus, :menu_id, :integer
  end
end
