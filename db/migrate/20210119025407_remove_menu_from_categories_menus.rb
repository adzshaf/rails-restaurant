class RemoveMenuFromCategoriesMenus < ActiveRecord::Migration[6.1]
  def change
    remove_column :categories_menus, :menu, :string
  end
end
