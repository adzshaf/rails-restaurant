class CreateMenuOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :menu_orders do |t|
      t.integer :quantity
      t.integer :order_id
      t.integer :menu_id

      t.timestamps
    end
  end
end
