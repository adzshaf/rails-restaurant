class RemoveCategoryFromCategoriesMenus < ActiveRecord::Migration[6.1]
  def change
    remove_column :categories_menus, :category, :string
  end
end
