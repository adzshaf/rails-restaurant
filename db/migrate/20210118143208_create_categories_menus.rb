class CreateCategoriesMenus < ActiveRecord::Migration[6.1]
  def change
    create_table :categories_menus do |t|
      t.string :category
      t.string :menu

      t.timestamps
    end
  end
end
