class Order < ApplicationRecord
    has_many :menu_orders, dependent: :destroy
    accepts_nested_attributes_for :menu_orders
end
