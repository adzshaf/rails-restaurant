class Menu < ApplicationRecord
    has_many :menu_orders, dependent: :destroy
    has_and_belongs_to_many :categories
    accepts_nested_attributes_for :categories
end
