class CategoryMenu < ActiveRecord::Base
    self.table_name = 'categories_menus'
  
    belongs_to :categories
    belongs_to :menus
  
end