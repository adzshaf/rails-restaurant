class CategoriesController < ApplicationController
  def new
    @category = Category.new
  end

  def index
    if params[:search].nil?
      @categories = Category.all
    else
      @parameter = params[:search].downcase  
      @categories = Category.where("lower(name) LIKE :search", search: "%#{@parameter}%")  
    end
  end

  def show
    @category = Category.find(params[:id])
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    redirect_to categories_path
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])

    if @category.update(category_params)
      redirect_to categories_path
    else
      flash[:error] = @category.errors.full_messages.[](0)
      render :edit
    end
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "Category created successfully."
      redirect_to action: :index
    else
      flash[:error] = @category.errors.full_messages.[](0)
      render action: :new
    end
  end

  private
  def category_params
    params.require(:category).permit(:name)
  end

end
