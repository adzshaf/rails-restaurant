class CustomersController < ApplicationController
    def new
        @customer = Customer.new
    end

    def edit
        @customer = Customer.find(params[:id])
    end

    def destroy
        @customer = Customer.find(params[:id])
        @customer.destroy

        redirect_to action: :index
    end

    def create
        @customer = Customer.new(customer_params)
        if @customer.save
          flash[:success] = "Customer created successfully."
          redirect_to action: :index
        else
          flash[:error] = @customer.errors.full_messages.[](0)
          render action: :new
        end
      end

    def update
        @customer = Customer.find(params[:id])

        if @customer.update(customer_params)
          redirect_to customers_path
        else
          flash[:error] = @customer.errors.full_messages.[](0)
          render :edit
        end
    end

    def show
        @customer = Customer.find(params[:id])
        @orders = Order.where(customer_id: params[:id])
    end

    def index
      if params[:search].nil?
        @customers = Customer.all
      else
        @parameter = params[:search].downcase  
        @customers = Customer.where("lower(customer_name) LIKE :search", search: "%#{@parameter}%")  
      end
    end

    private
    def customer_params
      params.require(:customer).permit(:customer_name, :phone_number)
    end
end
