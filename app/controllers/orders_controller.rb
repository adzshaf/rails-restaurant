class OrdersController < ApplicationController
    def index
      if params[:search].nil?
        @orders = Order.all
      else
        @parameter = params[:search].downcase  
        @customer = Customer.where("lower(customer_name) LIKE :search", search: "%#{@parameter}%").first

        if @customer.blank?
          @orders = []
        else
          @orders = Order.where(customer_id: @customer.id)
        end
      end
    end

    def new
      @customers = Customer.all
    end
    
    def new_by_customer
        @menus = Menu.all
        @order = Order.new
        @order.menu_orders.build
    end

    def show
      @order = Order.find(params[:id])
      @customer = Customer.find(@order.customer_id)
      @menu_orders = MenuOrder.where(order_id: @order.id)
    end

    def edit
      @order = Order.find(params[:id])
    end

    def destroy
      @order = Order.find(params[:id])
      @order.destroy
  
      redirect_to action: :index
    end

    def update
      @order = Order.find(params[:id])
  
      if @order.update(order_params)
        redirect_to action: :index
      else
        flash[:error] = @order.errors.full_messages.[](0)
        render :edit
      end
    end

    def create
      sum = 0
      params[:order][:menu_orders_attributes].each do |menu|
        sum += (Menu.find(menu.[](1)[:menu_id]).price.to_i * menu.[](1)[:quantity].to_i)
      end

      @order = Order.new(customer_id: params[:id], total_price: sum)

      if @order.save
        params[:order][:menu_orders_attributes].each do |menu|
          if !menu.[](1)[:quantity].blank? && menu.[](1)[:quantity] != "0"
            MenuOrder.create(menu_id: menu.[](1)[:menu_id], quantity: menu.[](1)[:quantity], order_id: @order.id)
          end
        end
        flash[:success] = "Order created successfully."
        redirect_to action: :index
      else
        flash[:error] = @order.errors.full_messages.[](0)
        render action: :new_by_customer
      end
    end

    private
    def order_params
      params.require(:order).permit(menu_orders_attributes: [:menu_id, :quantity])
    end
end
