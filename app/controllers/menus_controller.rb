class MenusController < ApplicationController
    def index
      if params[:search].nil?
        @menus = Menu.all
      else
        @parameter = params[:search].downcase  
        @menus = Menu.where("lower(menu_name) LIKE :search", search: "%#{@parameter}%")  

        if @menus.blank? 
          @parameter = params[:search].downcase  
          @categories = Category.where("lower(name) LIKE :search", search: "%#{@parameter}%")
  
          id = []
          @categories.each do |category|
            id.push(category.id)
          end
  
          menu_result = ActiveRecord::Base.connection.execute("SELECT menu_id FROM categories_menus where categories_menus.category_id in (#{id.join(",")})")
  
          @menus = []
          menu_result.each do |result|
            @menus.push(Menu.find(result["menu_id"]))
          end
        end
      end
    end

    def new
        @menu = Menu.new
    end

    def show
      @menu = Menu.find(params[:id])
    end

    def edit
      @menu = Menu.find(params[:id])
    end

    def destroy
      @menu = Menu.find(params[:id])
      @menu.destroy
  
      redirect_to action: :index
    end

    def update
      @menu = Menu.find(params[:id])
  
      if @menu.update(menu_params)
        redirect_to action: :index
      else
        flash[:error] = @menu.errors.full_messages.[](0)
        render :edit
      end
    end

    def create
        @menu = Menu.new(menu_params)
        p @menu.category_ids
        if @menu.save
          flash[:success] = "Menu created successfully."
          redirect_to action: :index
        else
          flash[:error] = @category.errors.full_messages.[](0)
          render action: :new
        end
      end

    private
    def menu_params
      params.require(:menu).permit(:menu_name, :price, category_ids: [])
    end
end
