# Food Order Management System

Access site here: https://restaurant-shafiya.herokuapp.com/

Features:
1. Menu management for maintaining menu items (food or beverage) information such as name, price and their categories.
2. Customer management for maintaining customer information such as name and phone.
3. Order management to record each customer transaction, the total amount they spend, transaction date, and also the menu items they order.

## How to run

```bash
bundle install

rails db:migrate

rails server
```

To run spec:
```bash
rspec
```