require 'rails_helper'

RSpec.describe Category, type: :model do
  it "has none to begin with" do
    expect(Category.count).to eq 0
  end

  it "will be not created if the category doesn't have name" do
    Category.create
    expect(Category.count).to eq 0
  end

  it "has one after adding one" do
    Category.create(name: "All Day Menu")
    expect(Category.count).to eq 1
  end

  it "has none after one was created in a previous example" do
    expect(Category.count).to eq 0
  end

  it "has category's name" do
    category = Category.create(name: "All Day Menu")
    expect(category.name).to eq "All Day Menu"
  end

  it "will be valid if name is string" do
    category = Category.create(name: "All Day Menu")
    expect(category.valid?).to eq true
  end

  it "will be not valid if name is nil" do 
    category = Category.create(name: nil)
    expect(category.valid?).to eq false
  end

  it "should not save category if name already exists" do 
    category1 = Category.create(name: "All Day Menu")
    category2 = Category.create(name: "All Day Menu")
    expect(Category.count).to eq 1
  end

  it "should return error message if name already exists" do 
    category1 = Category.create(name: "All Day Menu")
    category2 = Category.create(name: "All Day Menu")
    expect(category2.errors.full_messages.[](0)).to eq "Name is already taken"
  end
end
