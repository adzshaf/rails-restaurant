require 'rails_helper'

RSpec.describe Menu, type: :model do
  it "has none to begin with" do
    expect(Menu.count).to eq 0
  end

  it "has one after adding one" do
    Menu.create
    expect(Menu.count).to eq 1
  end

  it "has none after one was created in a previous example" do
    expect(Menu.count).to eq 0
  end

  it "should has name, price, and category" do
    breakfast = Category.create(name: "Breakfast")
    food = Category.create(name: "Food")
    menu = Menu.create(menu_name: "Pancake", price: 20000, category_ids: [breakfast.category_ids, food.category_ids])
    expect(menu.menu_name).to eq "Pancake"
    expect(menu.price).to eq 20000
    expect(menu.category).to eq [breakfast, food]
  end

  it "should not delete categories if menu is deleted" do
    breakfast = Category.create(name: "Breakfast")
    food = Category.create(name: "Food")
    menu = Menu.create(menu_name: "Pancake", price: 20000, category_ids: [breakfast.category_ids, food.category_ids])
    menu.destroy
    expect(Category.count).to eq 2
  end

  it "should delete menu's category if category is deleted" do
    breakfast = Category.create(name: "Breakfast")
    food = Category.create(name: "Food")
    menu = Menu.create(menu_name: "Pancake", price: 20000, category_ids: [breakfast.category_ids, food.category_ids])
    breakfast.destroy
    expect(menu.category.count).to eq 1
  end
end
