require 'rails_helper'

RSpec.describe "Categories", type: :request do

  describe "GET /new" do
    it "returns http success" do
      get "/categories/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /" do
    it "returns https success" do
      get "/categories"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /:id" do
    it "returns https success" do
      Category.create(name: "All Day Menu")
      get "/categories/1"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /:id/edit" do
    it "returns https success" do
      Category.create(name: "All Day Menu")
      get "/categories/1/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST category menu" do
    it "returns https success when successfully post category object" do
      params = { category: {
        name: "All Day Menu"
      }}

      post "/categories", :params => params
      expect(response).to have_http_status(:created)
    end

    it "will render new form if there is duplication error" do
      Category.create(name: "All Day Menu")
      Category.create(name: "Breakfast")

      params = { category: {
        name: "Breakfast"
      }}

      post "/categories", :params => params
      expect(response).to render_template(:new)
    end
  end

  describe "DELETE category menu" do
    it "returns https success" do
      Category.create(name: "All Day Menu")
      delete "/categories/1/"
      expect(response).to have_http_status(:no_content)
    end
  end

  describe "PATCH category menu" do
    it "returns https success" do
      Category.create(name: "All Day Menu")

      params = { category: {
        name: "Breakfast"
      }}

      patch "/categories/1", :params => params
      expect(response).to have_http_status(:no_content)
    end

    it "will render edit form if there is duplication error" do
      Category.create(name: "All Day Menu")
      Category.create(name: "Breakfast")

      params = { category: {
        name: "Breakfast"
      }}

      patch "/categories/1", :params => params
      expect(response).to render_template(:edit)
    end
  end

end
